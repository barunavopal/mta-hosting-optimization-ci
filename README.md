#project for letters2go
#mta-hosting-optimizer
Clone the project into locally then

a. install Docker in you local machine:

To run the Docker locally use the below commands:

1. $sudo docker build -f Dockerfile -t docker-mta-hosting-optimizer .
2. $sudo docker run -p 9091:9091 docker-mta-hosting-optimizer
3. $sudo docker build -f Dockerfile -t nginx-lb .
4. $sudo docker run -p 4001:4001 nginx-lb

the service will be accessible via:

http://localhost:9091/ipservice/ipconfig/valid #directly accessing the tomcat on port 9091

Now we have the load balancer here and docker-compose.yaml too

you can run the below commands to achieve the same:

1. sudo docker-compose build # will build the images
2. sudo docker-compose up # will start the services locally
3. sudo docker-compose down # this will stop the containers

We can scale up the backend server via the below command:
Run $sudo docker-compose up --scale backend1=2 --scale backend2=2 # this is start 4 backend server

We will also incorporate Rancer for deployment and scaling (to be updated: work in progress)

The application will be available via the below url
http://localhost:4001

Note: for running into local machine, we need to change the path of mounting the jar and nginx.conf file to ./backend/*.jar and ./load-balancer/nginx.conf. else the docker image wont be able to find the path

b. .gitlab-ci.yml has the pipeline built for the project

