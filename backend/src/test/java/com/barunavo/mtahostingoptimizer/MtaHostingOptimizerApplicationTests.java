package com.barunavo.mtahostingoptimizer;

import com.barunavo.mtahostingoptimizer.resource.IpConfig;
import com.barunavo.mtahostingoptimizer.resource.SampleDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MtaHostingOptimizerApplicationTests {

	@Value("${spring.application.threshold}")
	String threshold;

	@Test
	public void testThresholdValuePositive(){
		assertThat(threshold).isEqualTo("1");
	}

	@Test
	public void testThresholdValueBlank(){
		threshold = "";
		assertThat(threshold).isEqualTo("");
	}

	@Test
	public void testThresholdValueNegative(){
		threshold = "-1";
		assertThat(Integer.parseInt(threshold)).isEqualTo(-1);
	}

	@Test
	public void testThresholdValueNull(){
		threshold = null;
		assertThat(threshold).isEqualTo(null);
	}



	@Test
	public void testValidateIPMethod() {
		List<IpConfig> ipSampleLists = new ArrayList<>();
		Integer threshold = 2;
		ipSampleLists.addAll(new SampleDataProvider().getIpLists());
		assertThat(ipSampleLists).isExactlyInstanceOf(ArrayList.class);
		assertThat(ipSampleLists).isNotEqualTo(null);
		assertThat(ipSampleLists.size()).isGreaterThan(0);
		assertThat(threshold).isGreaterThan(1);
		assertThat(threshold).isNotEqualTo(null);
	}

	@Test
	public void testSaveHostToIPMethod(){
		Map<String, Set<String>> hostToIPMap = null;
		IpConfig config = null;
		Set<String> set = null;
		assertThat(hostToIPMap).isEqualTo(null);
		assertThat(config).isEqualTo(null);
		assertThat(set).isEqualTo(null);
		hostToIPMap = new HashMap<>();
		config = new IpConfig();
		set = new HashSet<>();
		config.setActive(true);
		config.setIp("127.0.0.1");
		config.setHostFqdn("test-host-1");
		set.add(config.getIp());
		hostToIPMap.put(config.getHostFqdn(),set);
		assertThat(hostToIPMap).isNotEmpty();
		assertThat(set).isNotEmpty();
		assertThat(config).isExactlyInstanceOf(IpConfig.class);
		assertThat(config.isActive()).isEqualTo(true);
	}

	@Test
	public void contextLoads() {

	}
}
