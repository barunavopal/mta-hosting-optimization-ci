package com.barunavo.mtahostingoptimizer.resource;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;


/****************** This is the Sample Data Provider class.*********************/

public class SampleDataProvider {

   private static final String IPv4_REGEX =
           "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                   "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                   "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                   "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

   private static final Pattern IPv4_PATTERN = Pattern.compile(IPv4_REGEX);

   //Object creation with the given Sample Data
   private final IpConfig config1 = new IpConfig("127.0.0.1 ", "mta-prod-1 ", true);
   private final IpConfig config2 = new IpConfig("127.0.0.2 ", "mta-prod-1 ", false);
   private final IpConfig config3 = new IpConfig("127.0.0.3 ", "mta-prod-2 ", true);
   private final IpConfig config4 = new IpConfig("127.0.0.4 ", "mta-prod-2 ", true);
   private final IpConfig config5 = new IpConfig("127.0.0.5 ", "mta-prod-2 ", false);
   private final IpConfig config6 = new IpConfig("127.0.0.6 ", "mta-prod-3 ", false);


   //Declared List to store the Objects of IpConfig

   public List<IpConfig> ipLists = Arrays.asList(config1,config2,config3, config4, config5, config6);


   //This method will validate the IPs while getting the values
   public static boolean validationForIP(String ip){

      if(ip == null){
         return false;
      }

      if(!IPv4_PATTERN.matcher(ip).matches()){
         return false;
      }

      return IPv4_PATTERN.matcher(ip).matches();

   }


   //Getter and Setter for the above declared List variable.
   public List<IpConfig> getIpLists() {
      ipLists.stream()
              .filter(ip -> validationForIP(ip.getIp()));
      return ipLists;
   }

   public void setIpLists(List<IpConfig> ipLists) {
      this.ipLists = ipLists;
   }
}
