package com.barunavo.mtahostingoptimizer.resource;

/****************** This is the POJO class.*********************/

public class IpConfig {

    //Declaring variables for the Sample Data
    private String ip;
    private String hostFqdn;
    private boolean active;

    public IpConfig() {
    }

    //Constructor declaration
    public IpConfig(String ip, String hostFqdn, boolean active) {
        this.ip = ip;
        this.hostFqdn = hostFqdn;
        this.active = active;
    }

    //Getters and Setters for the above declared variables
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostFqdn() {
        return hostFqdn;
    }

    public void setHostFqdn(String hostFqdn) {
        this.hostFqdn = hostFqdn;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}