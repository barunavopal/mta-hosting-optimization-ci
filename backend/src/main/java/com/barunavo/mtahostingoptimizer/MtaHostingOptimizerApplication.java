package com.barunavo.mtahostingoptimizer;

import com.barunavo.mtahostingoptimizer.resource.IpConfig;
import com.barunavo.mtahostingoptimizer.resource.SampleDataProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;



/****************** This is the starter class.*********************/

@SpringBootApplication
@RestController
@RequestMapping("/ipservice")
public class MtaHostingOptimizerApplication {


	/*
	 * This parameter is declared in the Spring application.yml
	 * */
	@Value("${spring.application.threshold}")
	private String threshold;


	/*
	 * This is the main method
	 * */
	public static void main(String[] args) {
		SpringApplication.run(MtaHostingOptimizerApplication.class, args);
	}


	/*
	 * This following method will provide all the Active IPs from
	 * the provided DataSet.
	 * */
	@GetMapping("/ipconfig/valid")
	public Set<String> getAllValidIPs() {
		//Getting all the sample data
		SampleDataProvider sampleDataProvider = new SampleDataProvider();

		//Initial Data are saved into a List Data Structure
		List<IpConfig> ipSampleLists = sampleDataProvider.getIpLists();

		//calling the 'validateIPs' method for the Active IPs
		Set<String> activeIPs = validateIPs(ipSampleLists,threshold());

		//Returning the active IPs
		return activeIPs;
	}


	/*
	 * This following method is create to make modularity in the main GET method.
	 * The inputs for this method are the Sample Data in a List format and the
	 * Threshold value which is set in the Spring application.yml. This method
	 * returns the Active IP Set.
	 * */
	private Set<String> validateIPs(List<IpConfig> ipSampleLists, Integer threshold) {
		//Declaring the return variable
		Set<String> ret = new HashSet<>();

		//Getting the hostFqdnSet from the SampleDataList
		Set<String> hostFqdnSet = ipSampleLists.stream()
				.map(ip -> ip.getHostFqdn())
				.collect(Collectors.toSet());

		//Declaring  the following variables for the internal manipulation
		Map<String, Set<String>> activeHostMap = new HashMap<>();
		Map<String, Set<String>> inActiveHostMap = new HashMap<>();
		Set<String> activeIpSet = new HashSet<>();
		Set<String> inActiveIpSet = new HashSet<>();

		//This step will set the values to the above declared variables
		for(IpConfig config : ipSampleLists){

			if(config.isActive()){

				//Calling the function 'saveHostToIP' to set the IP set to correspondence host
				saveHostToIP(activeHostMap, config, activeIpSet);
			} else {
				saveHostToIP(inActiveHostMap, config, inActiveIpSet);
			}

		}

		//Filtering out the InactiveIPs
		for(String s : hostFqdnSet){
			if(activeHostMap.get(s) != null && activeHostMap.get(s).size() > threshold){
				continue;
			}
			if(activeHostMap.get(s) != null && activeHostMap.get(s).size() < threshold){
				if(!ret.contains(s)) {
					ret.add(s);
				}
			}
			else{
				ret.add(s);
			}
		}

		return ret;
	}


	/*
	 * This function will take inputs as a Map , the IpConfig object and a Set
	 * and save  Set of IPs corresponding to a Host as there are one to many
	 * relationship present from hostname to IP.
	 * */
	private void saveHostToIP(Map<String, Set<String>> hostToIPMap, IpConfig config, Set<String> set){
		Set<String> internalSet = hostToIPMap.get(config.getHostFqdn());
		if(internalSet == null)
		{
			internalSet = new HashSet<>();
		}
		internalSet.add(config.getIp());

		set.add(config.getIp());

		hostToIPMap.put(config.getHostFqdn(), internalSet);
	}


	/*
	 * This function is used for providing the Threshold value in the Integer format
	 * as it is declared as String
	 * */
	public Integer threshold() {
		return Integer.parseInt(threshold);
	}

}

